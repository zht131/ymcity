# 应用介绍

YMCITY 是基于 fastadmin、uniapp 开发的同城便民信息系统，
目前支持微信小程序端、微信公众号端，
其他端需要自行适配差异化功能（如：支付、第三方登录等）
方便二次开发拓展！(温馨提示：谨防盗版)

# 前端开源仓库

[码云仓库： https://gitee.com/zht131/ymcity.git](https://gitee.com/zht131/ymcity.git)

# 体验预览

后台演示地址: [后台管理体验地址](https://demo.ybym.top/jODltiwyBH.php/ymcity/category?ref=addtabs)
账号: test 密码: admin888

前端演示联系作者

## 🔊 同道中人交流群

<table>
<thead>
<tr>
<th>QQ交流2群：971617215</th>
<th>QQ交流1群：971617215(已满)</th>
<th>作者微信：ZHT131572</th>
</tr>
</thead>
<tbody>
<tr>
<td align="center"><img src="https://wb.ybym.top/uploads/20240926/8d0615a21db9dcb12a9bc309c2ecc07a.jpg" width="120"></td>
<td align="center"><img src="https://wb.ybym.top/uploads/20240926/bd09400c0caf7624fcf4207781ce602f.jpg" width="120"></td>
<td align="center"><img src="https://wb.ybym.top/uploads/20240926/c339c352281a51bf5b42deb3225cb593.png" width="120"></td>
</tr>
</tbody>
</table>
